package com.service;


import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = "com")
public class TestBeanConfig {

}