package com.pettest;

import org.springframework.beans.factory.annotation.Autowired;

import com.model.Pets;
import com.model.User;
import com.service.UserServInf;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateUser extends SpringBootCucumberTest {
	@Autowired
	UserServInf userServInf;
	private User user;
	private Pets pets;
    private int id;
    private String name;
    private String password;
    private String conpassword;
    private int pet_id;
    private int pet_age;
    private String pet_name;
    private String place;
    
       @Given("^User wants to store the  data$")
    public void user_wants_to_store_the_data()throws Throwable {
          user = new User();
          pets=new Pets();
    }

    @When("^User enters id as (\\d+) and name as \"([^\"]*)\" and password as \"([^\"]*)\"$")
    public void user_enters_id_as_and_name_as_and_password_as(Integer id,String name,String password) throws Throwable {
        this.id=id;
        this.name=name;
        this.password=password;
       user.setUser_id(id);
       user.setUser_name(name);
        user.setUser_passwd(password);
    }
   
    @And("^User enters id as (\\d+) and petname as \"([^\"]*)\" and petage as (\\d+) and place as \"([^\"]*)\"$")
    public void user_enters_id_as_and_petname_as_and_petage_as_and_place_as(Integer pet_id,String name,Integer age,String place) throws Throwable {
       
        this.pet_id=pet_id;
        this.pet_name=name;
        this.pet_age=age;
        this.place=place;
       pets.setPet_id(pet_id);
       pets.setPet_name(pet_name);
       pets.setPet_place(place);
       pets.setUser(user);
    }
    @Then("^User and Pet saved successfully and it return as \"([^\"]*)\"$")
    public boolean user_and_pet_saved_successfully_and_it_return_as(String result) throws Throwable {
          userServInf.saveUser(user); 
          userServInf.savepet(user.getUser_id(), pets);;
        return true;
    }
}
